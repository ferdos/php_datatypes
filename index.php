<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $x = "Hello world!";
        $y = 'Hello world!';
        echo $x;
        echo "<br>";
        echo $y;
        ?>
        <?php
        $x = 5985;
        var_dump($x);
        ?>
        <?php
        $x = 10.365;
        var_dump($x);
        ?>
        <?php
        $x = true;
        $y = false;
        echo "$y";
        ?>
        <?php
        $cars = array("Volvo","BMW","Toyota");
        var_dump($cars);
        ?>
        <?php
        $x = "Hello world!";
        $x = null;
        var_dump($x);
        ?> 
    </body>
    </html>
